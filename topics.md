# Slide 1

Hello everybody! My name is Doncho Minkov and I will tell you how to create you components even better, using the Dumb-Smart principle.

# Slide 2

In this session I will:
1) briefly cover what components are
2) and what problem can arise when the application grows in size
3) I will tell you about smart and dumb components, or how we can separate our business logic from the presentation logic to create more reusable components.
4) At the end of the session, I will do a brief live demo to visualize all of the above


# Slide 3

Components in React are the Lego pieces of the application. With different pieces, you can achieve different tasks, yet with the same piece, many tasks can be done as well.

Components in React are two types:

- Function components that use only the `props`
- Class components, inheriting React.Component which can have props and state. The state is a very crucial part of a React application, because this is the application state.

# Slide 4

Now, knowing about Components, lets design a component that can do the following:
- Add a TODO
- List all TODOs that are done
- List all TODOs that are not done
- Mark TODO as DONE or not-done

It will look something like that:

_Show ugly code_

Using some separation, we will produce this:

_Show pretty code_

# Slide 5

Following the above, we can define:

- Dumb components, as components that are only pretty
    -   They do not run any complex/business logic.
    -   They only react on the provided `props`
    -   They can be used as regular HTML tags
    -   Can also be called Views (MVC)
- Smart components, as components that do not care about UI, but doing stuff
    -   They contain many smart and dumb components
    -   They do the stuff and tell the dubm components what to show
    -   Can also be called Controllers (MVC)


# Slide 6
The main idea is this: keep a single smart component responsible for the state of the application and many dumb components that only present data
