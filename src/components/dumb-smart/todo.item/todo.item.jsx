import React from 'react';

import './todo.item.css';

const TodoItem = (props) => (
    <label>
        <input type="checkbox" 
            value={props.todo.id} 
            onChange={props.onTodoStateChanged}/>
        <strong>
            {props.todo.text}
        </strong>
    </label>
);

export {
    TodoItem
};