import React from 'react';

const TodoAddForm = (props) => (
    <form>
        <label>
            <input value={props.todo.text}
                onChange={props.onNewTodoTextChanged} />
        </label>
        <button className="btn" onClick={props.onTodoAdded}>
            Save
        </button>
    </form>
);

export {
    TodoAddForm,
};