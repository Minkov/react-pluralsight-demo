import React, {
  Component
} from 'react';

import './App.css';

// import {
//   TodosApp,
// } from './components/one-component/todo.app.jsx';

import {
  TodosApp
} from './components/separated/todo.app.jsx';

// import {
//   TodosApp,
// } from './components/dumb-smart/todos.app/todos.app.jsx';

class App extends Component {
  render() {
    return ( 
      <TodosApp />
    );
  }
}

export default App;