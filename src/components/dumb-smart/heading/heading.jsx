import React from 'react';

import './heading.css';

const Heading = (props) => (
    <h1 className={`heading ${props.className}`}>
        {props.text}
    </h1>
);

export {
    Heading,
};