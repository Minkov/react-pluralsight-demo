import React from 'react';

import './todos.list.css';

import {
    TodoItem
} from '../todo.item/todo.item';

const TodosList = (props) => {
    return (
        <ul className="list-todos">
            {
                props.todos.map(todo =>
                    <li className="list-item" key={todo.id}>
                        <TodoItem todo={todo} 
                            onTodoStateChanged={props.onTodoStateChanged}/>
                    </li>
                )
            }
        </ul>
    );
}

export {
    TodosList
};