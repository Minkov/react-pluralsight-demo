import React, {
    Component
} from 'react';

import {
    TodosList,
} from '../todos.list/todos.list'

import {
    TodoAddForm,
} from '../todo.add.form/todo.add.form';

import {
    Heading,
} from '../heading/heading';

class TodosApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [{
                id: 1,
                text: 'Wash the dishes',
                isDone: false,
            }, {
                id: 2,
                text: 'Fix the bug',
                isDone: true,
            }, {
                id: 3,
                text: 'Buy grocery',
                isDone: false,
            }],
            todo: {
                text: '',
                isDone: false
            }
        };

        this.onTodoAdded = this.onTodoAdded.bind(this);
        this.onNewTodoTextChanged = this.onNewTodoTextChanged.bind(this);
        this.onTodoStateChanged = this.onTodoStateChanged.bind(this);
    }

    onNewTodoTextChanged(ev) {
        this.setState({
            todo: {
                text: ev.target.value,
                isDone: false,
            }
        });
    }

    onTodoAdded(ev) {
        const todos = this.state.todos;
        const lastId = this.state.todos.length > 0
            ? Math.max(...this.state.todos.map(todo => todo.id))
            : 0;

        const todo = {
            text: this.state.todo.text,
            id: lastId + 1,
            isDone: false,
        };

        todos.push(todo);

        this.setState({
            todos,
            todo: {
                text: '',
                isDone: false,
            }
        });

        ev.preventDefault();
    }

    onTodoStateChanged(ev) {
        const id = +ev.target.value;
        const todos = this.state.todos;
        const index = todos.findIndex(todo => todo.id === id);

        todos[index].isDone = !todos[index].isDone;
        this.setState({
            todos,
        });
    }

    render() {
        return (
            <div>
                <div>
                    <TodoAddForm todo={this.state.todo}
                        onNewTodoTextChanged={this.onNewTodoTextChanged}
                        onTodoAdded={this.onTodoAdded}/>
                </div>
                <div className="half">
                    <Heading text="Done:" className="done" />
                    <TodosList todos={this.state.todos.filter(todo => todo.isDone)}
                        onTodoStateChanged={this.onTodoStateChanged} />
                </div>
                <div className="half">
                    <Heading text="Not done:" className="not-done" />
                    <TodosList todos={this.state.todos.filter(todo => !todo.isDone)}
                        onTodoStateChanged={this.onTodoStateChanged} />
                </div>
            </div>
        );
    }
}

export {
    TodosApp
};