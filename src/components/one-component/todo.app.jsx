import React, { Component } from 'react';

import './todo.app.css';

class TodosApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [{
                id: 1,
                text: 'Wash the dishes',
                isDone: false,
            }, {
                id: 2,
                text: 'Fix the bug',
                isDone: true,
            }, {
                id: 3,
                text: 'Buy grocery',
                isDone: false,
            }],
            todo: {
                text: ''
            }
        };

        this.onTodoChangeState = this.onTodoChangeState.bind(this);
        this.onTodoTextChange = this.onTodoTextChange.bind(this);
        this.onTodoAdded = this.onTodoAdded.bind(this);
    }

    onTodoChangeState(ev) {
        const id = +ev.target.value;
        const todos = this.state.todos;
        const index = todos.findIndex(todo => todo.id === id);
        todos[index].isDone = !todos[index].isDone;
        this.setState({
            todos,
        });
    }

    onTodoTextChange(ev) {
        const text = ev.target.value;
        this.setState({
            todo: {
                text,
            }
        });
    }

    onTodoAdded() {
        const todos = this.state.todos;
        const lastId = todos.length === 0
            ? 0
            : Math.max(...todos.map(todo => todo.id));

        const todo = this.state.todo;
        todo.id = lastId + 1;
        todo.isDone = false;

        todos.push(todo);
        this.setState({
            todos,
            todo: {
                text: ''
            }
        });
    }

    render() {
        return (
            <div>
                <form>
                    <label>
                        <input value={this.state.todo.text}
                            onChange={this.onNewTodoTextChanged} />
                    </label>
                    <button className="btn" onClick={this.onTodoAdded}>
                        Save
                    </button>
                </form>
                <div className="half">
                    <h1 className="heading done" />
                    <ul className="list-todos">
                        {
                            this.state.todos.filter(todo => todo.isDone)
                                .map(todo =>
                                    <label>
                                        <input type="checkbox"
                                            value={todo.id}
                                            onChange={this.onTodoStateChanged} />
                                        <strong>
                                            {todo.text}
                                        </strong>
                                    </label>
                                )
                        }
                    </ul>
                </div>
                <div className="half">
                    <h1 className="heading not-done" />
                    <ul className="list-todos">
                        {
                            this.state.todos.filter(todo => !todo.isDone)
                                .map(todo =>
                                    <label>
                                        <input type="checkbox"
                                            value={todo.id}
                                            onChange={this.onTodoStateChanged} />
                                        <strong>
                                            {todo.text}
                                        </strong>
                                    </label>
                                )
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

export {
    TodosApp,
};